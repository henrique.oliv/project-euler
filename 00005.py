#Smallest multiple

#2520 is the smallest number that can be divided by each of the numbers 
# from 1 to 10 without any remainder.

#What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

# by intuition we know any number having 5,7,9,11,13,17,19,16 will be big so 33300000

inic = 232792500
found = False

while found == False:
    k=0
    for i in range(2,21):
        if inic%i==0:
            k=k+1
        else:
            k=0   
    if k==19: 
        print(inic) 
        found = True
    inic += 2   
    print(inic)
    