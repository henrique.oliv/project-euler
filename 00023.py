# Non-abundant sums

# A perfect number is a number for which the sum of its proper divisors is exactly equal to 
# the number. For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.

# A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.

# As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can 
# be written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown 
# that all integers greater than 28123 can be written as the sum of two abundant numbers. However, 
# this upper limit cannot be reduced any further by analysis even though it is known that the greatest
# number that cannot be expressed as the sum of two abundant numbers is less than this limit.

# Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.

import math

def divisors(n):
    divs = [1]
    for i in range(2,int(math.sqrt(n))+1):
        if n%i == 0:
            divs.append(i)
            divs.append(n/i)
    divs = list(dict.fromkeys(divs))            
    return divs

abundant = []
abundants_sum = []

for i in range(1, 28123+1):
    if sum(divisors(i)) > i: abundant.append(i)

for i in range(0, len(abundant) + 1):
    for j in range(i, len(abundant) ):
        s = (abundant[i] + abundant[j])
        if s < 28124:
            abundants_sum.append(s)
        
abundants_sum = list(dict.fromkeys(abundants_sum))     

abundant_non_sum = [x for x in range(0,28123+1)]
abundant_non_sum = [x for x in abundant_non_sum if x not in abundants_sum]

print(sum(abundant_non_sum))