#Large sum
   
#Work out the first ten digits of the sum of the following one-hundred 50-digit numbers.

import os
fileDir = os.path.dirname(os.path.realpath('__file__'))
fileDir = os.path.join(fileDir, '03.ProjectEuler/inputs/00013.txt')

with open(fileDir) as f:
    mtx = f.readlines()
mtx = [x.strip() for x in mtx] 

sum = 0
for n in mtx:
    sum += int(n)

print(str(sum)[0:10])