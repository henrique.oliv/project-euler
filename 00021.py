# Amicable numbers

# Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
# If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable numbers.

# For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

# Evaluate the sum of all the amicable numbers under 10000.


import math
import pandas as pd 


def divisors(n):
    divs = [1]
    for i in range(2,int(math.sqrt(n))+1):
        if n%i == 0:
            divs.append(i)
            divs.append(n/i)
    divs = list(dict.fromkeys(divs))            
    return divs


df = pd.DataFrame(columns=['n', 'amicable'])
obs = 10000
for n in range(1, obs + 1):
    df = df.append({'n': n, 'amicable': sum(divisors(n)) }, ignore_index=True)

result = pd.merge(df, 
                  df, 
                  left_on='n',
                  right_on='amicable',
                  how='left')
result = result.loc[(result['n_x']==result['amicable_y']) & (result['n_y']==result['amicable_x'])]
result = result.loc[result['n_x']!=result['amicable_x']]

print(result['amicable_x'].sum())