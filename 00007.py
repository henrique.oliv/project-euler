#10001st prime
   
#By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
#What is the 10.001st prime number?

#Sieve of Eratosthenes

from math import log

def nth_prime(n): 
    ub = int(n*log(n) + n*log(log(n))) 
    p = [*range(ub+1)] 
    for i in range(2, int(ub**.5)+1): 
        if p[i]: 
            for j in range(i**2, ub+1, i): 
                p[j] = False 
    return [k for k in p if k]


print(nth_prime(10001)[10001])
