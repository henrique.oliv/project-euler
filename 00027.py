#Quadratic primes

#Euler discovered the remarkable quadratic formula:
# n^2+n+41

# It turns out that the formula will produce 40 primes for the consecutive integer values 0≤n≤39
# However, when n=40,40^2+40+41=40(40+1)+41 is divisible by 41, and certainly when n=41,41^2+41+41
# is clearly divisible by 41.

#The incredible formula n^2−79n+1601 was discovered, which produces 80 primes for the consecutive 
# values 0≤n≤79. The product of the coefficients, −79 and 1601, is −126479.

#Considering quadratics of the form: n^2+a*n+b, where |a|<1000 and |b|≤1000 where |n| 
# is the modulus/absolute value of n |11|=11 and |−4|=4
#Find the product of the coefficients, a and b, for the quadratic expression that produces 
# the maximum number of primes  for consecutive values of n, starting with n=0

import math

n = 100000

def primes2(n):
    n, correction = n-n%6+6, 2-(n%6>1)
    sieve = [True] * (n//3)
    for i in range(1,int(n**0.5)//3+1):
      if sieve[i]:
        k=3*i+1|1
        sieve[      k*k//3      ::2*k] = [False] * ((n//6-k*k//6-1)//k+1)
        sieve[k*(k-2*(i&1)+4)//3::2*k] = [False] * ((n//6-k*(k-2*(i&1)+4)//6-1)//k+1)
    return [2,3] + [3*i+1|1 for i in range(1,n//3-correction) if sieve[i]]

primes = primes2(n)

print(len(primes))


    # find the first 78.5k primes in 1M numbers or 9.5K given 100K
max_a = 999 #maximum a coefficient
max_b = 999 #maximum b coefficient
# when n=0, b has to be prime, so b must be in list of primes less than 1K
b_list = [ x for x in primes if x < max_b] + [ x*-1 for x in primes if x < max_b] 
b_list.sort()

coefficient_product = 0 # we want a*b to be the number we return
max_primes_generated = 0 # we need to keep this meta data to wait for product
# we will need to look at negative coefficients too
for a in range(-1*max_a,max_a+1):
    #print(&quot;A is&quot;, a)
    for b in b_list:
        n=0
        while abs(n**2+a*n+b) in primes:
            n+=1
        if n > max_primes_generated:
            max_primes_generated=n
            coefficient_product=a*b
            print (max_primes_generated, coefficient_product)