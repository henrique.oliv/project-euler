#Special Pythagorean triplet

#A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
#a^2 + b^2 = c^2
#For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

#There exists exactly one Pythagorean triplet for which a + b + c = 1000.
#Find the product abc.


n = 1000

for a in range(1,n +1):
    for b in range(a, n + 1 -a):
        c = int((a**2 + b**2)**0.5)
        if (c == (n - a - b)) and (a**2 + b**2 == c**2):
            print(a,b,c, a*b*c)
