#Largest prime factor
   
#The prime factors of 13195 are 5, 7, 13 and 29.
#What is the largest prime factor of the number 600851475143 ?


d, x =600851475143, 2
while x <= d-1:
    if d%x==0:
        d=d/x
    x=x+1
print(x)